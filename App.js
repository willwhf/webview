import { View } from 'react-native';
import { WebView } from 'react-native-webview';
export default function App() {
  return (
    <View style={{flex: 1, marginTop: '8%'}}>
      <WebView source={{ uri: "https://www.radiocom.org.br/" }} />
    </View>
  );
}
